# frozen_string_literal: true

class GuitarsController < ApplicationController
  def index
    @guitars = Guitar.all
    render json: @guitars
  end

  def show
    @guitar = Guitar.find(params[:id])
    render json: @guitar
  end

  def create
    @guitar = Guitar.create(
      model: params[:model],
      country: params[:country]
    )
    render json: @guitar
  end

  def update
    @guitar = Guitar.find(params[:id])
    @guitar.update(
      model: params[:model],
      country: params[:country]
    )
    render json: @guitar
  end

  def destroy
    @guitars = Guitar.all
    @guitar = Guitar.find(params[:id])
    @guitar.destroy
    render json: @guitars
  end
end
