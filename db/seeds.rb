# frozen_string_literal: true

guitars_data = [
  { model: 'Fender Jaguar', country: 'Mexico' },
  { model: 'Gibson Explorer', country: 'USA' },
  { model: 'Epiphone Casino', country: 'USA' }
]

guitars_data.each do |guitar_data|
  Guitar.create(guitar_data)
end
